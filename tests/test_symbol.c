/* test_symbol.c: Symbol functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

static char *SYMBOLS[] = {
    "SP",
    "LCL",
    "ARG",
    "THIS",
    "THAT",
    NULL,
};

int main(int argc, char *argv[]) {
    struct symbol_t *s = NULL;

    for (char **sp = SYMBOLS; *sp; sp++) {
        s = symbol_prepend(s, *sp, sp - SYMBOLS);
    }

    symbol_print_all(s);

    for (char **sp = SYMBOLS; *sp; sp++) {
        if (symbol_find(s, *sp)) {
            printf("Found %s!\n", *sp);
        }
    }

    if (symbol_find(s, "PS") == NULL) {
        printf("Didn't Find PS!\n");
    }

    if (symbol_find(s, "DERP") == NULL) {
        printf("Didn't Find DERP!\n");
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
