/* test_register.c: 1-bit register functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

#define LIMIT 12

const bit_t INPUT[LIMIT] = {
    0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0
};

const bit_t LOAD[LIMIT] = {
    0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1
};

int main(int argc, char *argv[]) {
    bit_t r   = 0;
    bit_t clk = ClockCycle();

    for (int i = 0; i < LIMIT; i++) {
        r = RegisterCycle(r, clk, INPUT[i], LOAD[i]);
        printf("r=%d, clk=%d, in=%d, load=%d\n", r, clk, INPUT[i], LOAD[i]);
        clk = ClockCycle();
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
