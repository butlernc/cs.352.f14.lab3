/* test_clock.c: clock functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

#define LIMIT 12

int main(int argc, char *argv[]) {
    for (int i = 0; i < LIMIT; i++) {
        printf("clk=%d\n", ClockCycle());
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
