/* test_register16.c: 16-bit register functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

#define LIMIT 12

const bit_t LOAD[LIMIT] = {
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1
};

int main(int argc, char *argv[]) {
    word_t r   = 0;
    bit_t  clk = ClockCycle();

    for (word_t i = 0; i < LIMIT; i++) {
        r = Register16Cycle(r, clk, i, LOAD[i]);
        printf("r=%2u, clk=%2d, in=%2d, load=%2d\n", r, clk, i, LOAD[i]);
        clk = ClockCycle();
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
