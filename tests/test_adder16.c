/* test_adder16.c: 16-bit Adder functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

void TestAdder16(word_t a, word_t b) {
    word_t sum;
    bit_t  cout;
    char   as[BS_LEN];
    char   bs[BS_LEN];
    char   ss[BS_LEN];

    Adder16(a, b, 0, &sum, &cout);

    word_to_binary_string(a, as);
    word_to_binary_string(b, bs);
    word_to_binary_string(sum, ss);

    printf("  %s %4d\n", as, (int16_t)a);
    printf("+ %s %4d\n", bs, (int16_t)b);
    printf("  ----------------\n");
    printf("  %s %4d\n", ss, (int16_t)sum);
    putchar('\n');
}

int main(int argc, char *argv[]) {
    TestAdder16( 0,  0);

    TestAdder16( 0,  1);
    TestAdder16( 1,  0);

    TestAdder16( 0, -1);
    TestAdder16(-1,  0);

    TestAdder16( 1,  1);
    TestAdder16( 1, -1);
    TestAdder16(-1,  1);
    TestAdder16(-1, -1);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
