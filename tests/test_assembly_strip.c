/* test_assembly_strip.c: Assembly strip functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char *strip_comments(char *s);
extern char *strip_whitespace(char *s);

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];
    char *sp;

    strncpy(buffer, "", BUFSIZ);
    printf("strip_comments:   [%s] -> ", buffer);
    sp = strip_comments(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "// Comment", BUFSIZ);
    printf("strip_comments:   [%s] -> ", buffer);
    sp = strip_comments(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "A = M + D // Addition", BUFSIZ);
    printf("strip_comments:   [%s] -> ", buffer);
    sp = strip_comments(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "", BUFSIZ);
    printf("strip_whitespace: [%s] -> ", buffer);
    sp = strip_whitespace(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "// Comment", BUFSIZ);
    printf("strip_whitespace: [%s] -> ", buffer);
    sp = strip_whitespace(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "A = M + D // Addition", BUFSIZ);
    printf("strip_whitespace: [%s] -> ", buffer);
    sp = strip_whitespace(buffer);
    printf("[%s]\n", sp);

    strncpy(buffer, "This is Austen Ott's Idea // Get Him In IRC", BUFSIZ);
    printf("both:             [%s] -> ", buffer);
    sp = strip_whitespace(strip_comments(buffer));
    printf("[%s]\n", sp);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
