/* test_cpu.c: CPU functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define cpu_run(_msg, _reset, _instruction) \
    strcpy(buffer, _instruction); \
    printf("* Test %s: %s *\n", _msg, buffer); \
    instruction = assemble_line(buffer); \
    cpu = CPUStep(cpu, _reset, instruction, cpu.memory.data); \
    CPUDump(cpu, _reset, instruction, cpu.memory.data);

int main(int argc, char *argv[]) {
    struct cpu_t cpu;
    word_t instruction;
    char buffer[BUFSIZ];

    memset(&cpu, 0, sizeof(struct cpu_t));

    cpu_run("CPU reset", 1, "@0");

    cpu_run("PC inc", 0, "@0");
    cpu_run("PC inc", 0, "@0");

    cpu_run("A-instruction", 0, "@1");
    cpu_run("A-instruction", 0, "@100");

    cpu_run("C-instruction", 0, "D = 1");
    cpu_run("C-instruction", 0, "A = 1");
    cpu_run("C-instruction", 0, "M = -1");
    cpu_run("C-instruction", 0, "M = D");
    cpu_run("C-instruction", 0, "M = A");
    cpu_run("C-instruction", 0, "D = M");
    cpu_run("C-instruction", 0, "M = !D");
    cpu_run("C-instruction", 0, "M = !A");
    cpu_run("C-instruction", 0, "D = !M");
    cpu_run("C-instruction", 0, "M = -D");
    cpu_run("C-instruction", 0, "M = -A");
    cpu_run("C-instruction", 0, "D = -M");
    cpu_run("C-instruction", 0, "A = D + 1");
    cpu_run("C-instruction", 0, "D = A + 1");
    cpu_run("C-instruction", 0, "M = M + 1");
    cpu_run("C-instruction", 0, "A = D - 1");
    cpu_run("C-instruction", 0, "D = A - 1");
    cpu_run("C-instruction", 0, "M = M - 1");
    cpu_run("C-instruction", 0, "D = D + A");
    cpu_run("C-instruction", 0, "D = D + M");
    cpu_run("C-instruction", 0, "D = D - A");
    cpu_run("C-instruction", 0, "D = D - M");
    cpu_run("C-instruction", 0, "D = A - D");
    cpu_run("C-instruction", 0, "D = M - D");
    cpu_run("C-instruction", 0, "D = D & A");
    cpu_run("C-instruction", 0, "D = D & M");
    cpu_run("C-instruction", 0, "D = D | A");
    cpu_run("C-instruction", 0, "D = D | M");

    cpu_run("C-instruction", 0, "-1;JGT");
    cpu_run("C-instruction", 0, " 0;JGT");
    cpu_run("C-instruction", 0, " 1;JGT");

    cpu_run("C-instruction", 0, "-1;JEQ");
    cpu_run("C-instruction", 0, " 0;JEQ");
    cpu_run("C-instruction", 0, " 1;JEQ");
    cpu_run("C-instruction", 0, "-1;JGE");
    cpu_run("C-instruction", 0, " 0;JGE");
    cpu_run("C-instruction", 0, " 1;JGE");

    cpu_run("C-instruction", 0, "-1;JLT");
    cpu_run("C-instruction", 0, " 0;JLT");
    cpu_run("C-instruction", 0, " 1;JLT");

    cpu_run("C-instruction", 0, "-1;JNE");
    cpu_run("C-instruction", 0, " 0;JNE");
    cpu_run("C-instruction", 0, " 1;JNE");

    cpu_run("C-instruction", 0, " 1;JLE");
    cpu_run("C-instruction", 0, " 0;JLE");
    cpu_run("C-instruction", 0, "-1;JLE");

    cpu_run("C-instruction", 0, "-1;JMP");
    cpu_run("C-instruction", 0, " 0;JMP");
    cpu_run("C-instruction", 0, " 1;JMP");

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
