/* test_logic.c: Logic functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

typedef void bit_logic_func_t(bit_t, bit_t, bit_t *);
typedef void bit_mux_func_t(bit_t, bit_t, bit_t, bit_t *);

void TestBitLogicFunc(bit_logic_func_t *f, const char *s) {
    bit_t c;

    printf("%s:\n", s);
    printf("%2s | %2s || %2s\n", "a", "b", "f");

    for (int a = 0; a < 2; a++) {
        for (int b = 0; b < 2; b++) {
            f((bit_t)a, (bit_t)b, &c);
            printf("%2u | %2u || %2u\n", a, b, c);
        }
    }
}

void TestBitMuxFunc(bit_mux_func_t *f, const char *s) {
    bit_t c;

    printf("%s:\n", s);
    printf("%2s | %2s | %2s || %2s\n", "a", "b", "s", "f");

    for (int s = 0; s < 2; s++) {
        for (int a = 0; a < 2; a++) {
            for (int b = 0; b < 2; b++) {
                f((bit_t)a, (bit_t)b, (bit_t)s, &c);
                printf("%2u | %2u | %2u || %2u\n", a, b, s, c);
            }
        }
    }
}

int main(int argc, char *argv[]) {
    TestBitLogicFunc(Nand, "Nand");
    TestBitLogicFunc( And, "And");
    TestBitLogicFunc(  Or, "Or");
    TestBitLogicFunc( Xor, "Xor");

    TestBitMuxFunc(Mux2To1, "Mux2To1");

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
