/* test_disassembly.c: Disassembly test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define run_test(_msg, _instruction) \
    strcpy(cp, _instruction); \
    printf("* Test %s: %s *\n", _msg, cp); \
    instruction = assemble_line(cp); \
    word_to_binary_string(instruction, bs); \
    disassemble_word(instruction, ts); \
    printf("%s -> %s\n", bs, ts);

int main(int argc, char *argv[]) {
    struct cpu_t cpu;
    word_t instruction;
    char cp[BUFSIZ];
    char bs[BUFSIZ];
    char ts[BUFSIZ];

    memset(&cpu, 0, sizeof(struct cpu_t));

    run_test("A-instruction", "@1");
    run_test("A-instruction", "@100");

    run_test("C-instruction", "D = 0");
    run_test("C-instruction", "A = 1");
    run_test("C-instruction", "M = -1");
    run_test("C-instruction", "M = D");
    run_test("C-instruction", "M = A");
    run_test("C-instruction", "D = M");
    run_test("C-instruction", "M = !D");
    run_test("C-instruction", "M = !A");
    run_test("C-instruction", "D = !M");
    run_test("C-instruction", "M = -D");
    run_test("C-instruction", "M = -A");
    run_test("C-instruction", "D = -M");
    run_test("C-instruction", "A = D + 1");
    run_test("C-instruction", "D = A + 1");
    run_test("C-instruction", "M = M + 1");
    run_test("C-instruction", "A = D - 1");
    run_test("C-instruction", "D = A - 1");
    run_test("C-instruction", "M = M - 1");
    run_test("C-instruction", "D = D + A");
    run_test("C-instruction", "D = D + M");
    run_test("C-instruction", "D = D - A");
    run_test("C-instruction", "D = D - M");
    run_test("C-instruction", "D = A - D");
    run_test("C-instruction", "D = M - D");
    run_test("C-instruction", "D = D & A");
    run_test("C-instruction", "D = D & M");
    run_test("C-instruction", "D = D | A");
    run_test("C-instruction", "D = D | M");

    run_test("C-instruction", "-1;JGT");
    run_test("C-instruction", " 0;JGT");
    run_test("C-instruction", " 1;JGT");

    run_test("C-instruction", "-1;JEQ");
    run_test("C-instruction", " 0;JEQ");
    run_test("C-instruction", " 1;JEQ");

    run_test("C-instruction", "-1;JGE");
    run_test("C-instruction", " 0;JGE");
    run_test("C-instruction", " 1;JGE");

    run_test("C-instruction", "-1;JLT");
    run_test("C-instruction", " 0;JLT");
    run_test("C-instruction", " 1;JLT");

    run_test("C-instruction", "-1;JNE");
    run_test("C-instruction", " 0;JNE");
    run_test("C-instruction", " 1;JNE");

    run_test("C-instruction", " 1;JLE");
    run_test("C-instruction", " 0;JLE");
    run_test("C-instruction", "-1;JLE");

    run_test("C-instruction", "-1;JMP");
    run_test("C-instruction", " 0;JMP");
    run_test("C-instruction", " 1;JMP");

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
