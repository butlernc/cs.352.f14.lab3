/* test_dff.c: D Flip-Flop functions test */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>

#define LIMIT 12

const bit_t INPUT[LIMIT] = {
    0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0
};

int main(int argc, char *argv[]) {
    bit_t d   = 0;
    bit_t clk = ClockCycle();

    for (int i = 0; i < LIMIT; i++) {
        d = DFFCycle(d, clk, INPUT[i]);
        printf("d=%d, clk=%d, in=%d\n", d, clk, INPUT[i]);
        clk = ClockCycle();
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
