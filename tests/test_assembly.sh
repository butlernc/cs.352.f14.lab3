#!/bin/sh

set -e

export S_MACHINE_BATCH=1

echo Testing $1...

if [ -n "${S_MACHINE_BUIPJ}" ]; then
    SAM16_BASE=/data/scratch/cs.352.f14/lab3
else
    SAM16_BASE=./programs
fi

${SAM16_BASE}/s_assemble assembly/$1.asm assembly/$1.bin
${SAM16_BASE}/s_machine  assembly/$1.bin < ./tests/test_assembly_$1.input
