/* util.c: Utility functions */

#include "sam16.h"

#include <string.h>

/**
 * Convert word to binary string.
 *
 * @param w Input word.
 * @param s Output binary string.
 */
void word_to_binary_string(word_t w, char *s) {
    // TODO: Set each char in s to '0' or '1' based on bit in w
    for (size_t i = 0; i < WORD_SIZE; i++) {
        s[REV_BIT(i)] = '0' + GET_BIT(w, i);
    }

    // TODO: NULL terminate string
    s[WORD_SIZE] = 0;
}

/**
 * Convert binary string to word.
 *
 * @param s Input binary string.
 * @param w Output word.
 */
void binary_string_to_word(char *s, word_t *w) {
    // TODO: Convert up to WORD_SIZE characters
    size_t len = MIN(strlen(s), WORD_SIZE);

    // TODO: Initialize w to 0
    *w = 0;

    // TODO: Set each bit in w to the corresponding value in s
    for (size_t i = 0; i < len; i++) {
        if (s[i] == '1') {
            SET_BIT(*w, (len - i - 1), 1);
        }
    }
}

/**
 * bits to word: converts bit array to word
 *
 * @param b Input bit array.
 * @param w Output word.
 */
void bits_to_word(bit_t b[WORD_SIZE], word_t *w) {
    // TODO: Initialize w to 0
    *w = 0;

    // TODO: Set each bit in word to match bit array
    for (int i = 0; i < WORD_SIZE; i++) {
        SET_BIT(*w, i, b[i]);
    }
}

/**
 * reverse bits: reverses bit array.
 *
 * @param b Input bit array.
 */
void reverse_bits(bit_t b[WORD_SIZE]) {
    // TODO: swap the bits in the array
    for (int i = 0; i < WORD_SIZE/2; i++) {
        bit_t temp    = b[i];
        b[i]          = b[REV_BIT(i)];
        b[REV_BIT(i)] = temp;
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
