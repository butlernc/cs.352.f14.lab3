/* adder.c: Adder functions */

#include "sam16.h"

/**
 * Half Adder: s = sum, cout = carry out
 *
 * @param a    Input bit.
 * @param b    Input bit.
 * @param s    Output bit.
 * @param cout Output bit.
 */
void HalfAdder(bit_t a, bit_t b, bit_t *s, bit_t *cout) {
    // TODO: Implement in terms of 1-bit gates
    Xor(a, b, s);
    And(a, b, cout);
}

/**
 * Full Adder: s = sum, cout = carry out
 *
 * @param a    Input bit.
 * @param b    Input bit.
 * @param cin  Input bit.
 * @param s    Output bit.
 * @param cout Output bit.
 */
void FullAdder(bit_t a, bit_t b, bit_t cin, bit_t *s, bit_t *cout) {
    // TODO: Implement in terms of 1-bit gates
    bit_t xab;
    bit_t aab;
    bit_t abc;

    Xor(a, b, &xab);
    Xor(xab, cin, s);

    And(a, b, &aab);
    And(xab, cin, &abc);
    Or(aab, abc, cout);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
