/* logic.c: Logic functions */

#include "sam16.h"

/**
 * NAND: f = a NAND b
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Nand(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using C logical operators
    *f = !(a & b);
}

/**
 * NOT: f = NOT(a)
 *
 * @param a Input bit.
 * @param f Output bit.
 */
void Not(bit_t a, bit_t *f) {
    // TODO: Implement using only Nand
    Nand(a, a, f);
}

/**
 * AND: f = a AND b
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void And(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t c;

    Nand(a, b, &c);
    Nand(c, c, f);
}

/**
 * OR: f = a OR b
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Or(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t c;
    bit_t d;

    Nand(a, a, &c);
    Nand(b, b, &d);
    Nand(c, d, f);
}

/**
 * XOR: f = a XOR b
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param f Output bit.
 */
void Xor(bit_t a, bit_t b, bit_t *f) {
    // TODO: Implement using only Nand
    bit_t c;
    bit_t d;
    bit_t e;

    Nand(a, b, &c);
    Nand(a, c, &d);
    Nand(b, c, &e);
    Nand(d, e, f);
}

/**
 * 2to1 Mux: f = a if s == 0 else b
 *
 * @param a Input bit.
 * @param b Input bit.
 * @param s Select bit.
 * @param f Output bit.
 */
void Mux2To1(bit_t a, bit_t b, bit_t s, bit_t *f) {
    // TODO: Implement using And, Not, Or
    bit_t ns;
    bit_t as;
    bit_t bs;

    Not(s, &ns);

    And(a, ns, &as);
    And(b, s, &bs);
    Or(as, bs, f);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
