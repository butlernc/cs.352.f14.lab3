/* cpu.c: CPU functions */

#include "sam16.h"

/**
 * Decode instruction into CPU control signals before we execute ALU.
 *
 * @param cpu           Input CPU structure.
 * @param reset         Input reset bit.
 * @param instruction	Input instruction word.
 */
void CPUDecoderPre(struct cpu_t *cpu, bit_t reset, word_t instruction) {
    // TODO: cpu->am.select is 1 when we are using M in C-instruction
    And(1, GET_BIT(instruction, A), &cpu->am.select);
    // TODO: cpu->alu.{zx, nx, zy, ny, f, no} corresponds to C1, C2, C3, C4,
    // C5, C6
    cpu->alu.zx = GET_BIT(instruction, C1);
    cpu->alu.nx = GET_BIT(instruction, C2);
    cpu->alu.zy = GET_BIT(instruction, C3);
    cpu->alu.ny = GET_BIT(instruction, C4);
    cpu->alu.f  = GET_BIT(instruction, C5);
    cpu->alu.no = GET_BIT(instruction, C6);
}

/**
 * Decode instruction into CPU control signals after we execute ALU.
 *
 * @param cpu           Input CPU structure.
 * @param reset         Input reset bit.
 * @param instruction	Input instruction word.
 * @param zr            Input ALU zero output.
 * @param ng            Input ALU negative output.
 */
void CPUDecoderPost(struct cpu_t *cpu, bit_t reset, word_t instruction, bit_t zr, bit_t ng) {
    // TODO: We have a-instruction if bit T is 0, otherwise we have
    // c-instruction
    bit_t c_instruct = GET_BIT(instruction, T);
    bit_t a_instruct;
    Not(c_instruct, &a_instruct);
    
    // TODO: a.select is 1 when we we have an A-Instruction
    bit_t n_reset;
    Not(reset, &n_reset);
    And(a_instruct, n_reset, &(cpu->a.select));
    
    // TODO: a.load is 1 when we have A-instruction OR we have C-instruction
    // and D1 is active
    bit_t t, d1;
    d1 = GET_BIT(instruction, D1);
    And(c_instruct, d1, &t);
    Or(t, a_instruct, &(cpu->a.load));
    
    // TODO: d.load is 1 when we have C-instruction and D2 is active
    bit_t d2 = GET_BIT(instruction, D2);
    And(c_instruct, d2, &(cpu->d.load));
    
    // TODO: memory.write is 1 when we have C-instruction and D3 is active
    bit_t d3 = GET_BIT(instruction, D3);
    And(c_instruct, d3, &(cpu->memory.write));
    // TODO: pc.load is 1 when:
    //  - We have a C-instruction AND
    //  - We meet the jump condition
    
    bit_t jgt, jeq, jge, jlt, jne, jle, jmp;
    bit_t not_zr, not_ng;
    Not(zr, &not_zr);
    Not(ng, &not_ng);
    
    // TODO: jgt is 1 when J3 AND NOT(zr) AND NOT(ng)    
    bit_t temp1;
    And(GET_BIT(instruction, J3), not_zr, &temp1);
    And(temp1, not_ng, &jgt);
    
    // TODO: jeq is 1 when J2 AND zr    
    And(GET_BIT(instruction, J2), zr, &jeq);
    
    // TODO: jge is 1 when jgt OR jeq 
    Or(jgt, jeq, &jge);

    // TODO: jlt is 1 when J1 AND ng
    And(GET_BIT(instruction, J1), ng, &jlt);
    
    // TODO: jne is 1 when J1 AND J3 AND NOT(zr)
    bit_t temp2;
    And(GET_BIT(instruction, J1), GET_BIT(instruction, J3), &temp2);
    And(temp2, not_zr, &jne);
    
    // TODO: jle is 1 when jlt OR eq
    Or(jlt, jeq, &jle);
    
    // TODO: jmp is 1 when J1 AND J2 AND J3
    bit_t temp3;
    And(GET_BIT(instruction, J1), GET_BIT(instruction, J2), &temp3);
    And(temp3, GET_BIT(instruction, J3), &jmp);
        
    bit_t t1, t2, t3, t4, t5, t6;
    Or(jgt, jeq, &t1);
    Or(t1, jge, &t2);
    Or(t2, jlt, &t3);
    Or(t3, jne, &t4);
    Or(t4, jle, &t5);
    Or(t5, jmp, &t6);
    And(t6, c_instruct, &(cpu->pc.load));
}

/**
 * Execute one CPU cycle.
 *
 * @param cpu           Input/Output CPU structure.
 * @param clk		Input clock bit.
 * @param reset         Input reset bit.
 * @param instruction   Input instruction word.
 * @param memory        Input memory word.
 */
struct cpu_t CPUCycle(struct cpu_t cpu, bit_t clk, bit_t reset, word_t instruction, word_t memory) {
    // TODO: Do pre ALU decoding
    CPUDecoderPre(&cpu, reset, instruction);
    // TODO: Select between A register and Memory
    word_t alu_in;
    Mux2To116(cpu.a.data, memory, cpu.am.select, &alu_in);
    bit_t zr, ng;
    word_t out;
    // TODO: Perform ALU
    ALU16(cpu.d.data, alu_in, cpu.alu, &out, &zr, &ng);   
    // TODO: Do post ALU decoding
    CPUDecoderPost(&cpu, reset, instruction, zr, ng); 
    // TODO: Update D register
    cpu.d.data = Register16Cycle(cpu.d.data, clk, out, cpu.d.load);
    // TODO: Update Program Counter
    cpu.pc.data = Counter16Cycle(cpu.pc.data, clk, 1, cpu.pc.load, reset, cpu.a.data);
    // TODO: Wire cpu.memory.data
    cpu.memory.data = out;
    // TODO: Wire cpu.memory.address
    cpu.memory.address = cpu.a.data;
    // TODO: Update A register
    word_t a_in;
    Mux2To116(out, instruction, cpu.a.select, &a_in);
    cpu.a.data = Register16Cycle(cpu.a.data, clk, a_in, cpu.a.load);
    return cpu;
}

/**
 * Execute one CPU step.
 *
 * @param cpu           Input/Output CPU structure.
 * @param reset         Input reset bit.
 * @param instruction   Input instruction word.
 * @param memory        Input memory word.
 */
struct cpu_t CPUStep(struct cpu_t cpu, bit_t reset, word_t instruction, word_t memory) {
    // TODO: Update clk and call cpu_cycle (2x)
    bit_t clk = ClockCycle();
    cpu = CPUCycle(cpu, clk, reset, instruction, memory);
    clk = ClockCycle();
    cpu = CPUCycle(cpu, clk, reset, instruction, memory);
    return cpu;
}

/**
 * Dump CPU information to stdout.
 *
 * @param cpu           Input/Output CPU structure.
 * @param reset         Input reset bit.
 * @param instruction   Input instruction word.
 * @param memory        Input memory word.
 */
void CPUDump(struct cpu_t cpu, bit_t reset, word_t instruction, word_t memory) {
    char bs[BS_LEN];
    char buffer[BUFSIZ];

    printf("=============================================\n");
    printf("reset              = %d\n", reset);
    word_to_binary_string(instruction, bs);
    disassemble_word(instruction, buffer);
    printf("instruction        = %s (%-5s)\n", bs, buffer);
    word_to_binary_string(memory, bs);
    printf("memory             = %s (%5d)\n", bs, memory);
    printf("---------------------------------------------\n");
    word_to_binary_string(cpu.a.data, bs);
    printf("cpu.a.data         = %s (%5d)\n", bs, (int16_t)cpu.a.data);
    printf("cpu.a.select       = %d\n", cpu.a.select);
    printf("cpu.a.load         = %d\n", cpu.a.load);
    word_to_binary_string(cpu.d.data, bs);
    printf("cpu.d.data         = %s (%5d)\n", bs, (int16_t)cpu.d.data);
    printf("cpu.d.load         = %d\n", cpu.d.load);
    printf("cpu.am.select      = %d\n", cpu.am.select);
    printf("cpu.alu.zx         = %d\n", cpu.alu.zx);
    printf("cpu.alu.nx         = %d\n", cpu.alu.nx);
    printf("cpu.alu.zy         = %d\n", cpu.alu.zy);
    printf("cpu.alu.ny         = %d\n", cpu.alu.ny);
    printf("cpu.alu.f          = %d\n", cpu.alu.f);
    printf("cpu.alu.no         = %d\n", cpu.alu.no);
    word_to_binary_string(cpu.memory.data, bs);
    printf("cpu.memory.data    = %s (%5d)\n", bs, (int16_t)cpu.memory.data);
    word_to_binary_string(cpu.memory.address, bs);
    printf("cpu.memory.address = %s (%5d)\n", bs, cpu.memory.address);
    printf("cpu.memory.write   = %d\n", cpu.memory.write);
    word_to_binary_string(cpu.pc.data, bs);
    printf("cpu.pc.data        = %s (%5d)\n", bs, cpu.pc.data);
    printf("cpu.pc.load        = %d\n", cpu.pc.load);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
