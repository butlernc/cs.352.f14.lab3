/* disassembly.c: Disassembly Functions */

#include "sam16.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//#define DEBUG
//#define DEBUG_COMP
//#define TEST_INT

/**
 * Disassemble the A-instruction into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param buffer        Output buffer.
 */
void disassemble_a_instruction(word_t instruction, char *buffer) {
    // TODO:
    int temp = (int)instruction;
    //  Store the integer value of the instruction into the buffer.  Besure to
    //  include the "@".
    buffer[0] = '@';
    sprintf(&buffer[1],"%d", temp);
}

/**
 * Disassemble the C-instruction dest field into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param dest          Output dest field buffer.
 */
void disassemble_dest_field(word_t instruction, char *dest) {
    // TODO:
    //
    //  Check the D1, D2, and D3 fields of instruction and append the
    //  appropriate mnemonic ('A', 'D', 'M') to the dest buffer.
    struct {
        bit_t d1;
        bit_t d2;
        bit_t d3;
        char *instruction;
    } DestTable [] = {
        {0, 0, 0, NULL},
        {0, 0, 1, "M"},
        {0, 1, 0, "D"},
        {0, 1, 1, "MD"},
        {1, 0, 0, "A"},
        {1, 0, 1, "AM"},
        {1, 1, 0, "AD"},
        {1, 1, 1, "AMD"}
    };

    #ifdef DEBUG
        printf("in dest \n");
    #endif

    bit_t bits[3];
    bits[0] = GET_BIT(instruction, D1);
    bits[1] = GET_BIT(instruction, D2);
    bits[2] = GET_BIT(instruction, D3);
    #ifdef DEBUG
        printf("created bit array in dest \n");
    #endif
    int correct = 0;
    for(int i = 0; i < 8; i++) {
        correct = 0;
        for(int j = 0; j < 3; j++) {
            bit_t check;
            switch(j) {
                case 0:
                    check = DestTable[i].d1;
                    break;
                case 1:
                    check = DestTable[i].d2;
                    break;
                case 2:
                    check = DestTable[i].d3;
                    break;
            }
            
            if(bits[j] == check) {
                correct++;
            }
        }
        #ifdef DEBUG
            printf("before dest check, i: %d correct: %d \n \n", i, correct);
        #endif
        if(correct == 3) {
            if(DestTable[i].instruction == NULL) {
                break;
            }else{
                #ifdef DEBUG
                    printf("instruction: %s\n", DestTable[i].instruction);
                #endif
                snprintf(dest, BUFSIZ, "%s", DestTable[i].instruction);
                break;
            }
        }
    }
    #ifdef DEBUG
        printf("at end of dest \n");
    #endif 
}

struct {
    bit_t c1;
    bit_t c2;
    bit_t c3;
    bit_t c4;
    bit_t c5;
    bit_t c6;
    char *instruction;
} CompTable [] = {
    {1, 0, 1, 0, 1, 0, "0"},
    // TODO: Fill in remaining ALU operation.
    {1, 1, 1, 1, 1, 1, "1"},
    {1, 1, 1, 0, 1, 0, "-1"},
    {0, 0, 1, 1, 0, 0, "D"},
    {1, 1, 0, 0, 0, 0, "A"},
    {0, 0, 1, 1, 0, 1, "!D"},
    {1, 1, 0, 0, 0, 1, "!A"},
    {0, 0, 1, 1, 1, 1, "-D"},
    {1, 1, 0, 0, 1, 1, "-A"},
    {0, 1, 1, 1, 1, 1, "D+1"},
    {1, 1, 0, 1, 1, 1, "A+1"},
    {0, 0, 1, 1, 1, 0, "D-1"},
    {1, 1, 0, 0, 1, 0, "A-1"},
    {0, 0, 0, 0, 1, 0, "D+A"},
    {0, 1, 0, 0, 1, 1, "D-A"},
    {0, 0, 0, 1, 1, 1, "A-D"},
    {0, 1, 0, 1, 0, 1, "D|A"},
    {0, 0, 0, 0, 0, 0, "D&A"}
};

/**
 * Disassemble the C-instruction comp field into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param comp          Output comp field buffer.
 */
void disassemble_comp_field(word_t instruction, char *comp) {
    // TODO:
    //
    //  Declare bits c1, c2, c3, c4, c5, c6, and a, and then use GET_BIT to
    //  extract their values from the instruction.
    bit_t bits[7];
    bits[0] = GET_BIT(instruction, A);
    bits[1] = GET_BIT(instruction, C1);
    bits[2] = GET_BIT(instruction, C2);
    bits[3] = GET_BIT(instruction, C3);
    bits[4] = GET_BIT(instruction, C4);
    bits[5] = GET_BIT(instruction, C5);
    bits[6] = GET_BIT(instruction, C6);
    #ifdef DEBUG_COMP
        printf("comp: ");
        for(int i = 0; i < 7; i++) {
            printf("%d", bits[i]);
        }
        printf("\n");
    #endif
    //  For each entry in the CompTable, if all cX fields match the cX bits
    //  extracted from the instruction, then copy the instruction string from
    //  the CompTable entry to the comp buffer and exit the loop.
    int correct = 0;
    for(int i = 0; i < 18; i++) {
        correct = 0;
        for(int j = 1; j < 7; j++) {
            bit_t check;
            switch(j) {
                case 1:
                    check = CompTable[i].c1;
                    break;
                case 2:
                    check = CompTable[i].c2;
                    break;
                case 3:
                    check = CompTable[i].c3;
                    break;
                case 4:
                    check = CompTable[i].c4;
                    break;
                case 5:
                    check = CompTable[i].c5;
                    break;
                case 6:
                    check = CompTable[i].c6;
                    break;
            }

            if(bits[j] == check) {
                correct++;
            }
        }
        
        if(correct == 6) {
            snprintf(comp, BUFSIZ, "%s", CompTable[i].instruction);
            break;
        }
    }
    //  If the a bit is set, then search comp and replace all instances of 'A'
    //  with 'M'.
    if(bits[0]) {
        char* c;    
        while((c = strchr(comp, 'A')) != NULL) {
            *c = 'M';
        }
    }
}

/**
 * Disassemble the C-instruction jump field into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param jump          Output jump field buffer.
 */
void disassemble_jump_field(word_t instruction, char *jump) {
    // TODO:
    struct {
        bit_t j1;
        bit_t j2;
        bit_t j3;
        char *instruction;
    } JumpTable [] = {
        {0, 0, 0, NULL},
        {0, 0, 1, "JGT"},
        {0, 1, 0, "JEQ"},
        {0, 1, 1, "JGE"},
        {1, 0, 0, "JLT"},
        {1, 0, 1, "JNE"},
        {1, 1, 0, "JLE"},
        {1, 1, 1, "JMP"} 
    };
    //  Declare bits j1, j2, and j3, and then use GET_BIT to extract their
    //  values from the instruction.   
    bit_t bits[3];
    bits[0] = GET_BIT(instruction, J1);
    bits[1] = GET_BIT(instruction, J2);
    bits[2] = GET_BIT(instruction, J3);
    //  For each combination of j1, j2, and j3, check them and then store the
    int correct = 0;
    for(int i = 0; i < 8; i++) {
        correct = 0;
        for(int j = 0; j < 3; j++) {
            bit_t check;
            switch(j) {
                case 0:
                    check = JumpTable[i].j1;
                    break;
                case 1:
                    check = JumpTable[i].j2;
                    break;
                case 2:
                    check = JumpTable[i].j3;
                    break;
            }

            if(bits[j] == check) {
                correct++;
            }
        }

        if(correct == 3) {
            if(JumpTable[i].instruction == NULL) {
                break;
            }else{
                snprintf(jump, BUFSIZ, "%s", JumpTable[i].instruction);
                break;
            }
        }
    }
    //  appropriate jump command in the jump buffer.
}

/**
 * Disassemble the C-instruction into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param buffer        Output buffer.
 */
void disassemble_c_instruction(word_t instruction, char *buffer) {
    #ifdef DEBUG
        printf("Entered the disassemble c instruct \n");
    #endif
    //  Declare three strings dest, comp, and jump with BUFFER_SIZE.
    char dest[BUFSIZ];
    char comp[BUFSIZ];
    char jump[BUFSIZ];
    //  Call disassemble_X_field where X is dest, comp, and jump.
    disassemble_dest_field(instruction, dest);
    #ifdef DEBUG
        printf("Past dest: %s \n", dest);
    #endif
    disassemble_comp_field(instruction, comp);
    #ifdef DEBUG
        printf("Past comp: %s \n", comp);
    #endif
    disassemble_jump_field(instruction, jump);
    #ifdef DEBUG
        printf("Past jump: %s \n", jump);
    #endif
    //  If the instruction is a jump command, then store "comp;jump" to the
    if(GET_BIT(instruction, J1) || GET_BIT(instruction, J2) || GET_BIT(instruction, J3)) {
        snprintf(buffer, BUFSIZ, "%s;%s", comp, jump);
    //  buffer.  Otherwise, store "dest=comp" to the buffer.
    }else{
        int r = snprintf(buffer, BUFSIZ, "%s=%s", dest, comp);
        #ifdef TEST_INT
            printf("dest=comp r: %d \n", r);
        #endif
    }
}

/**
 * Disassemble the instruction into machine language.
 *
 * @param instruction   Instruction word to disassemble.
 * @param buffer        Output buffer.
 */
void disassemble_word(word_t instruction, char *buffer) {
    // TODO:
    //  Based on the T bit of the instruction, either call
    //  disassemble_c_instruction or call disassemble_a_instruction.
    if(GET_BIT(instruction, T)) {
        disassemble_c_instruction(instruction, buffer);
    }else{
        disassemble_a_instruction(instruction, buffer);
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
