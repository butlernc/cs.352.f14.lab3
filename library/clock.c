/* clock.c: Clock functions */

#include "sam16.h"

/**
 * Global Clock variable.
 */
bit_t Clock = 0;

/**
 * Cycles global Clock.
 *
 * @return Previous Clock value.
 */
bit_t ClockCycle(void) {
    // TODO: Negate the global Clock and return previous value.
    bit_t old = Clock;

    Not(Clock, &Clock);
    return old;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
