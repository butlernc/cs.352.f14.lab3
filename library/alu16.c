/* alu16.c: 16-bit ALU functions */

#include "sam16.h"

/**
 * ALU: out = f(x,y), zr = 1 iff out == 0, ng = 1 iff out < 0
 *
 * @param x   Input word.
 * @param y   Input word.
 * @param ctl Input control bits.
 * @param out Output word.
 * @param zr  Output bit.
 * @param ng  Output bit.
 */
void ALU16(word_t x, word_t y, struct alu_ctl_t ctl, word_t *out, bit_t *zr, bit_t *ng) {
    word_t zero = 0;
    word_t x0;
    word_t xN;
    word_t xX;
    word_t y0;
    word_t yN;
    word_t yY;
    word_t x_plus_y;
    word_t x_and_y;
    bit_t  cout;
    word_t oout;
    word_t nout;

    // TODO: Modify x input
    Mux2To116(x, zero, ctl.zx, &x0);
    Not16(x0, &xN);
    Mux2To116(x0, xN, ctl.nx, &xX);

    // TODO: Modify y input
    Mux2To116(y, zero, ctl.zy, &y0);
    Not16(y0, &yN);
    Mux2To116(y0, yN, ctl.ny, &yY);

    // TODO: Compute ADD and AND functions
    And16(xX, yY, &x_and_y);
    Adder16(xX, yY, 0, &x_plus_y, &cout);
    Mux2To116(x_and_y, x_plus_y, ctl.f, &oout);

    // TODO: Modify output
    Not16(oout, &nout);
    Mux2To116(oout, nout, ctl.no, out);

    // TODO: Detect negative
    And(GET_BIT(*out, WORD_SIZE - 1), 1, ng);

    // TODO: Detect zero
    Or16Way(*out, zr);
    Not(*zr, zr);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
