/* sam16_machine.c: Machine functions */

#include "sam16.h"

/**
 * Execute one Machine cycle.
 *
 * @param cpu	Input/Output CPU structure.
 * @param clk	Clock bit.
 * @param reset Reset bit.
 * @param rom	Instruction ROM array.
 * @param mem	Data memory array.
 */
struct cpu_t MachineCycle(struct cpu_t cpu, bit_t clk, bit_t reset, word_t *rom, word_t *mem) {
    // TODO: Call CPUCycle and Memory16Cycle
    int line_num = cpu.pc.data + 1;
    word_t line = rom[line_num];
    cpu = CPUCycle(cpu, clk, reset, line, *mem );
    *mem = Memory16Cycle(mem, clk, cpu.memory.data, cpu.memory.address, cpu.memory.write);
    
    return cpu;
}

/**
 * Execute one Machine step.
 *
 * @param cpu	Input/Output CPU structure.
 * @param reset Reset bit.
 * @param rom	Instruction ROM array.
 * @param mem	Data memory array.
 */
struct cpu_t MachineStep(struct cpu_t cpu, bit_t reset, word_t *rom, word_t *mem) {
    // TODO: call ClockCycle and MachineCycle (2x)
    bit_t clk = ClockCycle();
    MachineCycle(cpu, clk, reset, rom, mem);
    clk = ClockCycle();
    MachineCycle(cpu, clk, reset, rom, mem);
    return cpu;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
