/* symbol.c: Symbol table functions */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
* Create symbol with specified name and value.
* @param symbol    Name of symbol.
* @param value     Symbol value.
* @return Pointer to new symbol structure.
*/
struct symbol_t *symbol_create(const char *symbol, word_t value) {
    //  1. Allocate a symbol structure.  Return NULL if allocation fails.
    struct symbol_t *new_symbol = (struct symbol_t *)malloc(sizeof(struct symbol_t));
    //  2. Allocate and copy specified symbol string.
    new_symbol->name = strdup(symbol);
    //  3. Set the symbol value and next pointer.
    new_symbol->value = value;
    new_symbol->next = NULL;

    return new_symbol;
}

/**
* Prepend symbol to head of list.
* @param shead     Head of list.
* @param symbol    New symbol name.
* @param value     New symbol value.
* @return Pointer to new head of list.
*/
struct symbol_t *symbol_prepend(struct symbol_t *shead, const char *symbol, word_t value) {
    //  1. Create new symbol structure with symbol and value arguments.
    struct symbol_t *new_symbol = symbol_create(symbol, value);
    //  2. Set next field of new symbol to current head of list.
    new_symbol->next = shead;
    //  3. Return new head of list.
    return new_symbol;
}

/**
* Find symbol in list.
* @param shead     Head of list.
* @param symbol    Name of symbol to find.
* @return Pointer to symbol if found, otherwise NULL.
*/
struct symbol_t *symbol_find(struct symbol_t *shead, const char *symbol) {
    // TODO:
    //
    //  For each element in list, check of symbol is equal to the current
    //  element.  If so, then return current element, otherwise return NULL.
    struct symbol_t *current = shead;
    for(int i = 0; current != NULL; i++) {
        if(!strcmp((current->name),(symbol))) {
            return current;
        }else{
            current = current->next;
        }
    }
    return (NULL);
}

/**
* Print all symbols in list.
* @param shead     Head of list.
*/
void symbol_print_all(struct symbol_t *shead) {
    // TODO:
    //
    //  For each element in list, print out the current symbol name and value
    //  using the following function:
    struct symbol_t *current = shead;
    for(int i = 0; current != NULL; i++){
        printf("symbol=%s, value=%d\n", current->name, current->value);
        current = current->next;
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
