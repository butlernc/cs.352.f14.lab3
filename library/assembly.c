/* assembly.c: Assembly functions */

//#define DEBUG

#include "sam16.h"

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Global variables */
static FILE *InputStream        = NULL;
static FILE *OutputStream       = NULL;
static size_t LineCount         = 0;
static size_t MemoryCount       = 0;
static size_t ProgramCount      = 0;
static struct symbol_t *Symbols = NULL;
static char TempFile[]          = "/tmp/sa_XXXXXX";

/**
 * Open input and output streams.
 * @param   input_file  Path to input file.
 * @param   output_file Path to output file.
 */
void open_files(const char *input_file, const char *output_file) {
    if (streq(input_file, "-")) {
        int fd = mkstemp(TempFile);

        if (fd != -1 && (InputStream = fdopen(fd, "w+")) != NULL) {
            char buffer[BUFSIZ];

            while (fgets(buffer, BUFSIZ, stdin) != NULL) {
                fputs(buffer, InputStream);
            }
            fflush(InputStream);
        } else {
            fprintf(stderr, "Unable to open temporary file %s: %s\n", TempFile, strerror(errno));
            exit(1);
        }
    } else {
        InputStream = fopen(input_file, "r");
        if (InputStream == NULL) {
            fprintf(stderr, "Unable to open input file %s: %s\n", input_file, strerror(errno));
            exit(1);
        }
    }

    OutputStream = fopen(output_file, "w+");
    if (OutputStream == NULL) {
        fprintf(stderr, "Unable to open output file %s: %s\n", output_file, strerror(errno));
        exit(1);
    }
}

/**
 * Close input and output streams.
 */
void close_files() {
    fclose(InputStream);
    fclose(OutputStream);
    unlink(TempFile);
}

/**
 * Strip comments from string.
 * @param s String.
 */
char *strip_comments(char *s) {
    // TODO:
    //
    //  Find location of first "//".  If it exists, then set the character at
    //  that location to NUL.  Return s.
    char *comment = NULL;
    comment = strstr(s, "//");
    
    if(comment != NULL) {
        int size_s = strlen(s) - 1;
        int size_com = strlen(comment) - 1;
        s[size_s - size_com] = '\0';
    }
    return (s);
}

/**
 * Strip whitespace from string.
 * @param s String.
 */
char *strip_whitespace(char *s) {
    // TODO:
    //
    //  Using a "read" and "write" pointer, check if the character at the read
    //  pointer is not a space.  If it is not a space, then copy the character
    //  to the location pointed by the write pointer and advance the write
    //  pointer.  Do this until we have read all of the string by advancing the
    //  read pointer for every character.
    //
    //  Be sure to NUL terminate the string and return s.
    char *write;
    char *read;
    int index = 0;
    for(int i = 0; i < strlen(s); i++) {
        read = &(s[i]);
        if(!isspace(*read)) { 
            write = &(s[index]);
            strncpy(write, read, 1);
            index ++;
        }
    }
    s[index] = (char)0;
    return (s);
}

/**
 * Print instruction to stdout.
 * @param instruction Instruction word.
 */
void print_instruction(word_t instruction) {
    char bs[BS_LEN];

    word_to_binary_string(instruction, bs);
    for (int i = 0; i < WORD_SIZE; i++) {
        putchar(bs[i]);
        if ((i + 1) % 4 == 0 && i <( WORD_SIZE - 1)) {
            putchar(' ');
        }
    }
    putchar('\n');
}

/**
 * Assemble program symbols dispatch function.
 * @param line  Instruction string.
 */
void assemble_program_symbols(char *line) {
    char *symbol;
    word_t value;

    if (line[0] != '(') {
        return;
    }

    symbol = &line[1];
    symbol[strlen(symbol) - 1] = 0;

    if (symbol_find(Symbols, symbol)) {
        warn("Duplicate program symbol %s at line %lu", symbol, LineCount);
        return;
    }

    value   = ProgramCount;
    Symbols = symbol_prepend(Symbols, symbol, value);
    debug("Added Program Symbol %s = %d", symbol, value);
}

/**
 * Assemble memory symbols dispatch function.
 * @param line  Instruction string.
 */
void assemble_memory_symbols(char *line) {
    char *symbol;
    word_t value;

    if (line[0] != '@') {
        return;
    }

    symbol = &line[1];
    if (symbol_find(Symbols, symbol) || isdigit(symbol[0])) {
        return;
    }

    value   = MEMORY_OFFSET + MemoryCount++;
    Symbols = symbol_prepend(Symbols, symbol, value);
    debug("Added Memory Symbol %s = %d", symbol, value);
}

/**
 * Assemble A-instruction.
 * @param line  Instruction string.
 */
word_t assemble_a_instruction(char *line) {
    // TODO:
    //
    //  Extract the name from line.  Use symbol_find to see if the name is in
    //  the Symbols list.  If it is, then instruction is the value of that
    //  symbol.  Otherwise, instruction is value of name converted into an
    //  integer.  Return instruction.
    int r = 0;
    if(isdigit(line[0])) {
        r = (word_t)atoi(line);
        #ifdef DEBUG
            printf("@ instruction value: %d\n", r);
        #endif 
        return r;
    }else{
        struct symbol_t *new_sym = symbol_find(Symbols, line);
        if(new_sym != NULL) {
            r = new_sym->value;
            #ifdef DEBUG
                printf("@ instruction value: %d, symbol: %s\n", r, new_sym->name);
            #endif
            return r;
        }else{
            return 0;
        }
    }
    return (0);
}

/**
 * Parse C-instruction.
 * @param line  Instruction string.
 * @param dest  Output dest field string.
 * @param comp  Output comp field string.
 * @param jump  Output jump field string.
 */
void parse_c_instruction(char *line, char **dest, char **comp, char **jump) {
    #ifdef DEBUG
        printf("c instruction line: %s\n", line); 
    #endif
    //  1. Set dest, comp, and jump pointers to NULL.
    *dest = NULL;
    *comp = NULL;
    *jump = NULL;
    //  2. Search for ';'.  If found, then split the line in two and set the
    //  jump and comp pointers.
    char *n = NULL;

    n = strip_whitespace(line);
    //cut string at ';'
    n = strchr(n, ';');
    if(n != NULL) {
        *jump = n+1;
        *n = (char)0;
        *comp = line; 
    }
    //  3. Search for '='.  If found, then split the line in two and set the
    //  dest and comp pointers.
    n = strip_whitespace(line);
    //cut string at '='
    n = strchr(n, '=');
    if(n != NULL) {//check to see if there was an '='
        *comp = n+1;
        *n = (char)0;
        *dest = line;
    }
}


#define set_dest_bits(b, d1, d2, d3) \
    b[D1] = (bit_t)(d1); \
    b[D2] = (bit_t)(d2); \
    b[D3] = (bit_t)(d3);

/**
 * Assemble dest field bits
 * @param b     Bit array.
 * @param dest  Dest field string.
 */
void assemble_dest_bits(bit_t *b, const char *dest) {
    // TODO:
    //
    //  If dest is NULL, then return.
    #ifdef DEBUG
        printf("dest: %s\n", dest);
    #endif
    if(dest == NULL) {
        set_dest_bits(b, 0, 0, 0);
    //  Otherwise, use set_dest_bits to set the dest field bits (b) based on
    //  the input dest string.
    } else if(!strcmp(dest,"M")) {
        set_dest_bits(b, 0, 0, 1);
    } else if(!strcmp(dest,"D")) {
        set_dest_bits(b, 0, 1, 0);
    } else if(!strcmp(dest,"MD")) {
        set_dest_bits(b, 0, 1, 1);
    } else if(!strcmp(dest,"A")) {
        set_dest_bits(b, 1, 0, 0);
    } else if(!strcmp(dest,"AM")) {
        set_dest_bits(b, 1, 0, 1);
    } else if(!strcmp(dest,"AD")) {
        set_dest_bits(b, 1, 1, 0);
    } else if(!strcmp(dest,"AMD")) {
        set_dest_bits(b, 1, 1, 1);
    }
}

#define set_comp_bits(b, c1, c2, c3, c4, c5, c6) \
    b[C1] = (bit_t)(c1); \
    b[C2] = (bit_t)(c2); \
    b[C3] = (bit_t)(c3); \
    b[C4] = (bit_t)(c4); \
    b[C5] = (bit_t)(c5); \
    b[C6] = (bit_t)(c6);

/**
 * Assemble comp field bits
 * @param b     Bit array.
 * @param comp  Comp field string.
 */
void assemble_comp_bits(bit_t *b, const char *comp) {
    int a = 0;
    #ifdef DEBUG
        printf("comp: %s\n", comp);
    #endif
    //  If comp is NULL, then return.
    if(comp == NULL) {
        set_comp_bits(b, 1, 0, 1, 0, 1, 0);
    //  Otherwise, use set_comp_bits to set the comp field bits (b) based on
    //  the input comp string.
    }else if(!strcmp(comp,"0")) {
        set_comp_bits(b, 1, 0, 1, 0, 1, 0);
    }else if(!strcmp(comp,"1")) {
        set_comp_bits(b, 1, 1, 1, 1, 1, 1);
    }else if(!strcmp(comp,"-1")) {
        set_comp_bits(b, 1, 1, 1, 0, 1, 0);
    }else if(!strcmp(comp, "D")) {
        set_comp_bits(b, 0, 0, 1, 1, 0, 0);
    }else if(!strcmp(comp,"A") || !strcmp(comp,"M")) {
        set_comp_bits(b, 1, 1, 0, 0, 0, 0);
        a = ((!strcmp(comp,"M"))?1:0);
    }else if(!strcmp(comp,"!D")) {
        set_comp_bits(b, 0, 0, 1, 1, 0, 1);
    }else if(!strcmp(comp,"!A") || !strcmp(comp,"!M")) {
        set_comp_bits(b, 1, 1, 0, 0, 0, 1);
        a = ((!strcmp(comp,"!M"))?1:0);
    }else if(!strcmp(comp,"-D")) {
        set_comp_bits(b, 0, 0, 1, 1, 1, 1);
    }else if(!strcmp(comp,"-A") || !strcmp(comp,"-M")) {
        set_comp_bits(b, 1, 1, 0, 0, 1, 1);
        a = ((!strcmp(comp,"-M"))?1:0);
    }else if(!strcmp(comp,"D+1")) {
        set_comp_bits(b, 0, 1, 1, 1, 1, 1);
    }else if(!strcmp(comp,"A+1") || !strcmp(comp,"M+1")) {
        set_comp_bits(b, 1, 1, 0, 1, 1, 1);
        a = ((!strcmp(comp,"M+1"))?1:0);
    }else if(!strcmp(comp,"D-1")) {
        set_comp_bits(b, 0, 0, 1, 1, 1, 0);
    }else if(!strcmp(comp,"A-1") || !strcmp(comp,"M-1")) {
        set_comp_bits(b, 1, 1, 0, 0, 1, 0);
        a = ((!strcmp(comp, "M-1"))?1:0);
    }else if(!strcmp(comp,"D+A") || !strcmp(comp,"D+M")) {
        set_comp_bits(b, 0, 0, 0, 0, 1, 0);
        a = ((!strcmp(comp, "D+M"))?1:0);
    }else if(!strcmp(comp,"D-A") || !strcmp(comp,"D-M")) {
        set_comp_bits(b, 0, 1, 0, 0, 1, 1);
        a = ((!strcmp(comp, "D-M"))?1:0);
    }else if(!strcmp(comp,"A-D") || !strcmp(comp,"M-D")) {
        set_comp_bits(b, 0, 0, 0, 1, 1, 1);
        a = ((!strcmp(comp, "M-D"))?1:0);
    }else if(!strcmp(comp,"D&A") || !strcmp(comp,"D&M")) {
        set_comp_bits(b, 0, 0, 0, 0, 0, 0);
        a = ((!strcmp(comp, "D&M"))?1:0);
    }else if(!strcmp(comp,"D|A") || !strcmp(comp,"D|M")) {
        set_comp_bits(b, 0, 1, 0, 1, 0, 1);
        a = ((!strcmp(comp, "D|M"))?1:0);
    }
    //  Also remember to set the A bit field.
    b[A] = (bit_t)a;
    //  Emit a warning if the comp field is unknown.
}

#define set_jump_bits(b, j1, j2, j3) \
    b[J1] = (bit_t)(j1); \
    b[J2] = (bit_t)(j2); \
    b[J3] = (bit_t)(j3);

/**
 * Assemble jump field bits
 * @param b     Bit array.
 * @param jump  Jump field string.
 */
void assemble_jump_bits(bit_t *b, const char *jump) {
    //  If jump is NULL, then return.
    #ifdef DEBUG
        printf("jump: %s\n", jump);
    #endif
    if(jump == NULL) {
        set_jump_bits(b, 0, 0, 0);
    //  Otherwise, use set_jump_bits to set the jump field bits (b) based on
    //  the input jump string.
    }else if(!strcmp(jump,"JGT")) {
        set_jump_bits(b, 0, 0, 1);
    }else if(!strcmp(jump,"JEQ")) {
        set_jump_bits(b, 0, 1, 0);
    }else if(!strcmp(jump,"JGE")) {
        set_jump_bits(b, 0, 1, 1);
    }else if(!strcmp(jump,"JLT")) {
        set_jump_bits(b, 1, 0, 0);
    }else if(!strcmp(jump,"JNE")) {
        set_jump_bits(b, 1, 0, 1);
    }else if(!strcmp(jump,"JLE")) {
        set_jump_bits(b, 1, 1, 0);
    }else if(!strcmp(jump,"JMP")) {
        set_jump_bits(b, 1, 1, 1);
    }else{
    //  Emit a warning if the jump field is unknown.
        printf("UNKNOWN JUMP \n");
    }
}

/**
 * Assemble C-instruction.
 * @param line  Instruction string.
 */
word_t assemble_c_instruction(char *line) {
    //  1. Declare a bit array called instruction_bits of WORD_SIZE and initialize
    //  it for a C-instruction.
    bit_t instruction_bits[WORD_SIZE];
    instruction_bits[15] = 1;
    instruction_bits[14] = 1;
    instruction_bits[13] = 1;
    //  2. Declare an instruction word.
    word_t *instruction_word = (word_t*)malloc(sizeof(word_t));
    //  3. Declare and initialize a dest, comp, and jump field pointer.
    char **dest = (char**)malloc(sizeof(char*));
    char **comp = (char**)malloc(sizeof(char*));
    char **jump = (char**)malloc(sizeof(char*));
    // TODO: Call parse_c_instruction on the line, dest, comp, and jump fields.
    parse_c_instruction(line, dest, comp, jump);
    // TODO: Call assemble_*_bits functions for dest, comp, jump.
    assemble_dest_bits(instruction_bits, *dest);
    assemble_comp_bits(instruction_bits, *comp);
    assemble_jump_bits(instruction_bits, *jump);
    // TODO: Translate bit array into word using bits_to_word.
    bits_to_word(instruction_bits, instruction_word);
    return *instruction_word;
}

/**
 * Assemble line.
 * @param line Instruction string.
 */
int assemble_line(char *line) {
    // TODO:
    //
    //  Select on the first character in line:
    //  If first character is '(', then we do nothing.
    #ifdef DEBUG
        printf("line start: %s\n", line);
    #endif
    if(line[0] == '(') {    
    //  If first character is '@', then we assemble an A-instruction.
    }else if(line[0] == '@') {
       return assemble_a_instruction(line+1);
    //  Otherwise, we assemble a C-instruction.
    }else{
        return assemble_c_instruction(line);
    }
    return (0);
}

/**
 * Assemble program dispatch function.
 * @param line Instruction string.
 */
void assemble_program(char *line) {
    int instruction;
    char first = line[0] ;
    instruction = assemble_line(line);
    if (instruction >= 0 && first != '(') {
        fwrite(&instruction, sizeof(word_t), 1, OutputStream);
        print_instruction(instruction);
    }
}

/**
 * Assemble stream using dispatch function.
 * @param func Dispatch function.
 */
void assemble_stream(dispatch_func_t func) {
    char buffer[BUFSIZ];
    char *line;

    LineCount    = 0;
    MemoryCount  = 0;
    ProgramCount = 0;

    rewind(InputStream);

    while (fgets(buffer, BUFSIZ, InputStream)) {
        LineCount++;

        line = strip_comments(buffer);
        line = strip_whitespace(line);

        if (strlen(line) == 0)
            continue;

        func(line);

        if (line[0] != '(') {
            ProgramCount++;
        }
    }
}

/**
 * Assemble input file and store into output file
 * @param input_file    Path to input file.
 * @param output_file   Path to output file.
 */
void assemble(const char *input_file, const char *output_file) {
    open_files(input_file, output_file);
    assemble_stream(assemble_program_symbols);
    assemble_stream(assemble_memory_symbols);
    assemble_stream(assemble_program);
    close_files();
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
