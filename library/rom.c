/* rom.c: ROM functions */

#include "sam16.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

/**
 * Load binary program into ROM.
 * 
 * @param m	    ROM word array.
 * @param rom_path  Path to rom file.
 * @return Whether or not loading the ROM was successful.
 */
int load_rom(word_t m[ROM_SIZE], const char *rom_path) 
{
    FILE *fs;
    int result;

    fs = fopen(rom_path, "r");
    if (fs == NULL) {
    	warn("Unable to open ROM %s: %s", rom_path, strerror(errno));
    	return false;
    }

    result = fread(m, sizeof(word_t), ROM_SIZE, fs);
    debug("Read %d bytes from %s", result, rom_path);
    fclose(fs);
    return (result > 0);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
