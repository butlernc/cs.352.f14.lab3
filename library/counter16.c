/* counter16.c: 16-bit Counter functions */

#include "sam16.h"

/**
 * Decoder that translates the three counter inputs into two output select bits:
 *
 *  s = 00  Reset
 *  s = 01  Load
 *  s = 10  Increment
 *  s = 11  Pass-through
 *
 * @param inc	Increment control bit.
 * @param load	Load control bit.
 * @param reset Control bit.
 * @param s     Output select bits.
 */
void Counter16Decoder(bit_t inc, bit_t load, bit_t reset, bit_t s[2]) {
    // TODO:
    //
    //	Construct a decoder such that the following is output:
    //
    //	    00 Reset
    //	    01 Load
    //	    10 Increment
    //	    11 Pass-through

    bit_t incN;
    bit_t loadN;
    bit_t resetN;
    bit_t incNOrload;

    // TODO: construct !inc, !load, !reset
    Not(inc, &incN);
    Not(load, &loadN);
    Not(reset, &resetN);

    // TODO: construct !inc OR Load
    Or(incN, load, &incNOrload);

    // TODO: construct (load AND !reset) OR (!inc AND !load AND !reset)
    And(resetN, incNOrload, &s[0]);

    // TODO: (!load AND !reset)
    And(loadN, resetN, &s[1]);
}

/**
 * Simulates a 16-bit counter.
 *
 * @param c	Internal counter state.
 * @param clk   Clock.
 * @param inc	Increment control bit.
 * @param load	Load control bit.
 * @param reset Control bit.
 * @param in	Input word.
 */
word_t Counter16Cycle(word_t c, bit_t clk, bit_t inc, bit_t load, bit_t reset, word_t in) {
    // TODO: Implement a counter cycle by using a Counter16Decoder,
    // Adder16, Mux4To116, and Register16Cycle.

    bit_t s[2];
    bit_t cout;
    word_t rvalue;
    word_t c_plus_1;
    word_t zero = 0;

    Counter16Decoder(inc, load, reset, s);
    Adder16(c, 1, 0, &c_plus_1, &cout);
    Mux4To116(zero, in, c_plus_1, c, s, &rvalue);

    return Register16Cycle(c, clk, rvalue, 1);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
