/* register16.c: 16-bit register functions */

#include "sam16.h"

/**
 * Simulates a 16-bit register cycle.
 *
 * @param r	Register word.
 * @param clk	Clock.
 * @param in	Input word.
 * @param load  Load bit.
 */
word_t Register16Cycle(word_t r, bit_t clk, word_t in, bit_t load) {
    // TODO: Use 1-bit registers to implement a 16-bit register
    bit_t b[WORD_SIZE];
    for (int i = 0; i < WORD_SIZE; i++) {
        b[i] = RegisterCycle(GET_BIT(r, i), clk, GET_BIT(in, i), load);
    }
    bits_to_word(b, &r);
    return r;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
