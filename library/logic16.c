/* logic16.c: 16-bit Logic functions */

#include "sam16.h"

/**
 * NAND16: f = a NAND b
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Nand16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t c[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        Nand(GET_BIT(a, i), GET_BIT(b, i), &c[i]);
    }   

    bits_to_word(c, f); 
}

/**
 * NOT16: f = NOT(a)
 *
 * @param a Input word.
 * @param f Output word.
 */
void Not16(word_t a, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t b[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        Not(GET_BIT(a, i), &b[i]);
    }   

    bits_to_word(b, f); 
}

/**
 * AND16: f = a AND b
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void And16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t c[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        And(GET_BIT(a, i), GET_BIT(b, i), &c[i]);
    }   

    bits_to_word(c, f); 
}

/**
 * OR16: f = a OR b
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Or16(word_t a, word_t b, word_t *f) {
    // TODO: implement in terms of 1-bit equivalent
    bit_t c[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        Or(GET_BIT(a, i), GET_BIT(b, i), &c[i]);
    }   

    bits_to_word(c, f); 
}

/**
 * OR16Way: f = w[0] OR w[1] OR ... OR w[15]
 *
 * @param w Input word.
 * @param f Output bit.
 */
void Or16Way(word_t w, bit_t *f) {
    *f = GET_BIT(w, 0);
    for (int i = 1; i < WORD_SIZE; i++) {
        Or(*f, GET_BIT(w, i), f);
    }
}

/**
 * XOR16: f = a XOR b
 *
 * @param a Input word.
 * @param b Input word.
 * @param f Output word.
 */
void Xor16(word_t a, word_t b, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t c[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        Xor(GET_BIT(a, i), GET_BIT(b, i), &c[i]);
    }   

    bits_to_word(c, f); 
}

/**
 * 2To1 Mux16: f = a if s == 0 else b
 *
 * @param a Input word.
 * @param b Input word.
 * @param s Select bit.
 * @param f Output word.
 */
void Mux2To116(word_t a, word_t b, bit_t s, word_t *f) {
    // TODO: Implement in terms of 1-bit equivalent
    bit_t c[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        Mux2To1(GET_BIT(a, i), GET_BIT(b, i), s, &c[i]);
    }   

    bits_to_word(c, f); 
}

/**
 * 4To1 Mux16: f = a if s == 0 else b
 *
 * @param a Input word.
 * @param b Input word.
 * @param c Input word.
 * @param d Input word.
 * @param s Select bits.
 * @param f Output word.
 */
void Mux4To116(word_t a, word_t b, word_t c, word_t d, bit_t s[2], word_t *f) {
    // TODO: Implement 4to1 16-bit mux using 3 Mux2To116's.
    word_t m0; 
    word_t m1; 

    Mux2To116(a, b, s[0], &m0);
    Mux2To116(c, d, s[0], &m1);
    Mux2To116(m0, m1, s[1], f); 
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
