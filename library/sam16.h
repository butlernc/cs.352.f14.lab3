/* sam16.h: Simple 16-bit Machine */

#ifndef SAM16_H
#define SAM16_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/* Constants */
#define WORD_SIZE       16
#define BS_LEN          (WORD_SIZE + 1)
#define UWORD_MAX       65535
#define WORD_MIN        -32768
#define WORD_MAX        32767

#define ROM_SIZE        (WORD_MAX)
#define MEM_SIZE        (WORD_MAX)
#define MEMORY_OFFSET   16

/* C-Instruction format fields */
#define T   15
#define A   12
#define C1  11
#define C2  10
#define C3  9
#define C4  8
#define C5  7
#define C6  6
#define D1  5
#define D2  4
#define D3  3
#define J1  2
#define J2  1
#define J3  0

typedef enum {
    ALU_AND,
    ALU_ADD,
} alu_func_t;

/* Type definitions */
typedef bool        bit_t;
typedef uint16_t    word_t;
typedef void dispatch_func_t(char *);

struct alu_ctl_t {
    bit_t zx;               /**< Zero X input control bit. */
    bit_t nx;               /**< Not X input control bit. */
    bit_t zy;               /**< Zero Y input control bit. */
    bit_t ny;               /**< Not Y input control bit. */
    bit_t f;                /**< Function control bit. */
    bit_t no;               /**< Not output control bit. */
};

struct cpu_t {
    struct {
        word_t data;        /**< Address register data */
        bit_t  select;      /**< Address register select */
        bit_t  load;        /**< Address register load */
    } a;                    /**< Address register */
    struct {
        word_t data;        /**< Data register data */
        bit_t  load;        /**< Data register load */
    } d;                    /**< Data register */
    struct {
        bit_t select;       /**< Address/Memory select */
    } am;
    struct alu_ctl_t  alu;  /**< Alu control bits */
    struct {
        word_t data;        /**< Memory output data */
        word_t address;     /**< Memory address */
        bit_t  write;       /**< Memory write */
    } memory;
    struct {
        word_t data;        /**< Program counter data */
        bit_t  load;        /**< Program counter load */
    } pc;                   /**< Program counter */
};

struct symbol_t {
    char    *name;          /**< Symbol name */
    word_t   value;         /**< Symbol value */
    struct symbol_t *next;  /**< Pointer to next symbol */
};

/* Macros */
#ifdef DEBUG
#define debug(M, ...)       fprintf(stderr, "[DEBUG] %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define debug(M, ...)
#endif

#define warn(M, ...)        fprintf(stderr, "[WARN ] %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define MIN(a, b)           ((a) < (b) ? (a) : (b))
#define MAX(a, b)           ((a) > (b) ? (a) : (b))

#define GET_BIT(w, n)       ((w >> (n)) & 1)
#define SET_BIT(w, n, v)    (w |= ((v) << (n)))
#define REV_BIT(n)          (WORD_SIZE - 1 - (n))

#define streq(s0, s1)       (strcmp(s0, s1) == 0)

/* Global variables */
extern bit_t Clock;

/* Utility functions */
void word_to_binary_string(word_t w, char *s);
void binary_string_to_word(char *s, word_t *w);
void bits_to_word(bit_t b[WORD_SIZE], word_t *w);
void word_to_bits(word_t w, bit_t b[WORD_SIZE]);
void reverse_bits(bit_t b[WORD_SIZE]);

/* Logic functions */
void Nand(bit_t a, bit_t b, bit_t *f);
void Not(bit_t a, bit_t *f);
void And(bit_t a, bit_t b, bit_t *f);
void Or(bit_t a, bit_t b, bit_t *f);
void Xor(bit_t a, bit_t b, bit_t *f);
void Mux2To1(bit_t a, bit_t b, bit_t s, bit_t *f);

/* 16-bit Logic functions */
void Nand16(word_t a, word_t b, word_t *f);
void Not16(word_t a, word_t *f);
void And16(word_t a, word_t b, word_t *f);
void Or16(word_t a, word_t b, word_t *f);
void Or16Way(word_t w, bit_t *f);
void Xor16(word_t a, word_t b, word_t *f);
void Mux2To116(word_t a, word_t b, bit_t s, word_t *f);
void Mux4To116(word_t a, word_t b, word_t c, word_t d, bit_t s[2], word_t *f);

/* Adder functions */
void HalfAdder(bit_t a, bit_t b, bit_t *s, bit_t *cout);
void FullAdder(bit_t a, bit_t b, bit_t cin, bit_t *s, bit_t *cout);

/* 16-bit Adder functions */
void Adder16(word_t a, word_t b, bit_t cin, word_t *s, bit_t *cout);

/* 16-bit ALU functions */
void ALU16(word_t a, word_t b, struct alu_ctl_t ctl, word_t *out, bit_t *zr, bit_t *ng);

/* Clock functions */
bit_t ClockCycle(void);

/* DFF functions */
bit_t DFFCycle(bit_t d, bit_t clk, bit_t in);

/* Register functions */
bit_t RegisterCycle(bit_t r, bit_t clk, bit_t in, bit_t load);

/* 16-bit Register functions */
word_t Register16Cycle(word_t r, bit_t clk, word_t in, bit_t load);

/* 16-bit Memory functions */
word_t Memory16Cycle(word_t *m, bit_t clk, word_t in, word_t addr, bit_t load);

/* 16-bit Counter functions */
word_t Counter16Cycle(word_t c, bit_t clk, bit_t inc, bit_t load, bit_t reset, word_t in);

/* Symbol functions */
struct symbol_t *symbol_create(const char *name, word_t value);
struct symbol_t *symbol_prepend(struct symbol_t *shead, const char *name, word_t value);
struct symbol_t *symbol_find(struct symbol_t *shead, const char *name);
void symbol_print_all(struct symbol_t *shead);

/* Assembly functions */
int assemble_line(char *line);
void assemble(const char *input_file, const char *output_file);

/* Disassembly functions */
void disassemble_word(word_t word, char *buffer);

/* ROM functions */
int load_rom(word_t *rom, const char *rom_file);

/* CPU functions */
void CPUDump(struct cpu_t cpu, bit_t reset, word_t instruction, word_t memory);
struct cpu_t CPUStep(struct cpu_t cpu, bit_t reset, word_t instruction, word_t memory);
struct cpu_t CPUCycle(struct cpu_t cpu, bit_t clk, bit_t reset, word_t instruction, word_t memory);

/* Machine functions */
struct cpu_t MachineStep(struct cpu_t cpu, bit_t reset,  word_t *rom, word_t *mem);
struct cpu_t MachineCycle(struct cpu_t cpu, bit_t clk, bit_t reset, word_t *rom, word_t *mem);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
