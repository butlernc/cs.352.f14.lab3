/* adder16.c: 16-bit Adder functions */

#include "sam16.h"

/**
 * 16-bit Adder: s = sum
 *
 * @param a    Input bit.
 * @param b    Input bit.
 * @param cin  Input bit.
 * @param s    Output bit.
 * @param cout Output bit.
 */
void Adder16(word_t a, word_t b, bit_t cin, word_t *s, bit_t *cout) {
    // TODO: Implement in terms of 1-bit adders
    bit_t sum[WORD_SIZE];

    for (int i = 0; i < WORD_SIZE; i++) {
        FullAdder(GET_BIT(a, i), GET_BIT(b, i), cin, &sum[i], cout);
        cin = *cout;
    }

    bits_to_word(sum, s);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
