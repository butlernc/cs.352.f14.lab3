# Variables
CC	 = gcc
CFLAGS	 = -Wall -std=gnu99 -g -I$(shell pwd)/library
LDFLAGS	 = -L$(shell pwd)
LIBS	 =

LIBRARIES= library/adder.c \
	   library/adder16.c \
	   library/alu16.c \
	   library/assembly.c \
	   library/clock.c \
	   library/counter16.c \
	   library/cpu.c \
	   library/dff.c \
	   library/disassembly.c \
	   library/logic.c \
	   library/logic16.c \
	   library/machine.c \
	   library/memory16.c \
	   library/register.c \
	   library/register16.c \
	   library/rom.c \
	   library/symbol.c \
	   library/util.c
LIBRARY_OBJECTS	= $(LIBRARIES:.c=.o)
LIBRARY_HEADER	= library/sam16.h

PROGRAMS = programs/alu \
	   programs/s_assemble \
	   programs/s_machine \
	   programs/timer \
	   programs/tt
PROGRAM_OBJECTS = $(PROGRAMS:=.o)

TESTS    = tests/test_adder \
	   tests/test_adder16 \
	   tests/test_alu16 \
	   tests/test_assembly_strip \
	   tests/test_clock \
	   tests/test_counter16 \
	   tests/test_cpu \
	   tests/test_dff \
	   tests/test_disassembly \
	   tests/test_logic \
	   tests/test_logic16 \
	   tests/test_memory16 \
	   tests/test_register \
	   tests/test_register16 \
	   tests/test_symbol \
	   tests/test_util
TEST_OBJECTS = $(TESTS:=.o)

LIBRARY	 = library/libsam16.a

# Rules

all:	$(LIBRARY) $(TESTS) $(PROGRAMS)

debug:  CFLAGS += -DDEBUG
debug:  $(LIBRARY) $(TESTS) $(PROGRAMS)

doc:	Doxyfile
	doxygen

%.o: %.c $(LIBRARY_HEADER)
	$(CC) $(CFLAGS) -o $@ -c $<

$(LIBRARY): $(LIBRARY_OBJECTS)
	$(AR) rcs $(LIBRARY) $(LIBRARY_OBJECTS)

$(TESTS): %: %.o $(LIBRARY) $(LIBRARY_HEADER)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBRARY) $(LIBS)

$(PROGRAMS): %: %.o $(LIBRARY) $(LIBRARY_HEADER)
	$(CC) $(LDFLAGS) -o $@ $< $(LIBRARY) $(LIBS)

clean:
	rm -f $(LIBRARY) $(PROGRAM_OBJECTS) $(TEST_OBJECTS) $(LIBRARY_OBJECTS) $(TESTS) $(PROGRAMS)

test:	$(TESTS) $(PROGRAMS)
	./tests/test_util 	    | diff -u - ./tests/test_util.output
	./tests/test_logic 	    | diff -u - ./tests/test_logic.output
	./tests/test_logic16 	    | diff -u - ./tests/test_logic16.output
	./tests/test_adder	    | diff -u - ./tests/test_adder.output
	./tests/test_adder16	    | diff -u - ./tests/test_adder16.output
	./tests/test_alu16	    | diff -u - ./tests/test_alu16.output
	./tests/test_clock	    | diff -u - ./tests/test_clock.output
	./tests/test_dff	    | diff -u - ./tests/test_dff.output
	./tests/test_register	    | diff -u - ./tests/test_register.output
	./tests/test_register16	    | diff -u - ./tests/test_register16.output
	./tests/test_memory16	    | diff -u - ./tests/test_memory16.output
	./tests/test_counter16      | diff -u - ./tests/test_counter16.output
	./tests/test_symbol	    | diff -u - ./tests/test_symbol.output
	./tests/test_assembly_strip | diff -u - ./tests/test_assembly_strip.output
	./programs/s_assemble assembly/add.asm assembly/add.bin | diff -u - assembly/add.output
	./programs/s_assemble assembly/max.asm assembly/max.bin | diff -u - assembly/max.output
	./programs/s_assemble assembly/sum.asm assembly/sum.bin | diff -u - assembly/sum.output
	./tests/test_disassembly    | diff -u - ./tests/test_disassembly.output
	./tests/test_cpu            | diff -u - ./tests/test_cpu.output
