/* timer.c: Timer Chip */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/**
 * Timer structure
 */
struct timer_t {
    word_t seconds;
    word_t minutes;
    bit_t  s_inc;
    bit_t  s_reset;
    bit_t  s_load;
    bit_t  m_inc;
    bit_t  m_reset;
    bit_t  m_load;
};

/**
 * Tests if two words are equal.
 *
 * @param w0 Input word.
 * @param w1 Input word.
 * @param f  Output bit.
 */
void Equal16(word_t w0, word_t w1, bit_t *f) {
    word_t diff;

    Xor16(w0, w1, &diff);
    Or16Way(diff, f);
    Not(*f, f);
}

/**
 * Updates Timer control signals.
 */
void TimerController(struct timer_t *timer) {
    // TODO: Set timer seconds control signals
    timer->s_inc   = 1;
    timer->s_load  = 0;
    Equal16(timer->seconds, 59, &(timer->s_reset));

    // TODO: Set timer minutes control signals
    timer->m_inc   = timer->s_reset;
    timer->m_load  = 0;
    Equal16(timer->minutes, 59, &(timer->m_reset));
    And(timer->m_reset, timer->s_reset, &(timer->m_reset));
}

int main(int argc, char *argv[]) {
    struct timer_t timer = {0};
    bit_t clk;

    while (true) {
        // TODO: Pass timer to controller to update signals
        TimerController(&timer);

        // TODO: Update clock, seconds counter, and minutes counter
        clk = ClockCycle();
        timer.seconds = Counter16Cycle(timer.seconds, clk, timer.s_inc, timer.s_load, timer.s_reset, 0);
        timer.minutes = Counter16Cycle(timer.minutes, clk, timer.m_inc, timer.m_load, timer.m_reset, 0);

        if (clk) {
            if (getenv("TIMER_DEBUG")) {
                printf("Time: %02d:%02d\n", timer.minutes, timer.seconds);
            } else {
                printf("\rTime: %02d:%02d", timer.minutes, timer.seconds);
                fflush(stdout);
                sleep(1);
            }
        }
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
