/* s_machine.c: SAM16 machine */

#include "sam16.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CPU_STEP_SIZE 1
#define MEM_DUMP_SIZE 8

int main(int argc, char *argv[]) {
    word_t rom[ROM_SIZE];
    word_t mem[MEM_SIZE];
    char buffer[BUFSIZ];
    struct cpu_t cpu;
    int command_count = 0;

    if (argc > 2) {
        fprintf(stderr, "usage: %s [rom_file]\n", argv[0]);
        return EXIT_FAILURE;
    }

    memset(rom, 0, sizeof(word_t)*ROM_SIZE);
    memset(mem, 0, sizeof(word_t)*MEM_SIZE);
    memset(&cpu, 0, sizeof(struct cpu_t));
    memset(buffer, 0, sizeof(char)*BUFSIZ);

    if (argc == 2) {
        load_rom(rom, argv[1]);
    }

    while (true) {
        char rom_file[BUFSIZ];
        char bs[WORD_SIZE];
        size_t bsize;
        int address = 0;
        int dsize = MEM_DUMP_SIZE;
        int steps = CPU_STEP_SIZE;
        int value = 0;

        if (getenv("S_MACHINE_BATCH") == NULL) {
            printf("sam16[%6X]> ", command_count);
        }

        if (fgets(buffer, BUFSIZ, stdin) == NULL) {
            break;
        }

        bsize = strlen(buffer);
        if (bsize <= 1) {
            continue;
        }

        buffer[bsize - 1] = 0;
        if (streq(buffer, "exit") || streq(buffer, "quit") || streq(buffer, "q")) {
            break;
        } else if (strncmp(buffer, "echo", 4) == 0) {
            char *sp = strchr(buffer + 4, ' ');
            if (sp != NULL) {
                printf("%s\n", sp + 1);
            }
        } else if (streq(buffer, "step") || sscanf(buffer, "step %d", &steps) == 1 || streq(buffer, "s") || sscanf(buffer, "s %d", &steps) == 1) {
            for (int i = 0; i < steps; i++) {
                cpu = MachineStep(cpu, 0, rom, mem);
            }
            if (getenv("S_MACHINE_BATCH") != NULL) {
                continue;
            }
            CPUDump(cpu, 0, rom[cpu.pc.data], mem[cpu.memory.address]);
        } else if (streq(buffer, "dump") || streq(buffer, "d")) {
            CPUDump(cpu, 0, rom[cpu.pc.data], mem[cpu.memory.address]);
        } else if (streq(buffer, "reset")) {
            cpu = MachineStep(cpu, 1, rom, mem);
            if (getenv("S_MACHINE_BATCH") != NULL) {
                continue;
            }
            CPUDump(cpu, 0, rom[cpu.pc.data], mem[cpu.memory.address]);
        } else if (sscanf(buffer, "load %s", rom_file) == 1 || sscanf(buffer, "l %s", rom_file) == 1) {
            if (load_rom(rom, rom_file)) {
                cpu = MachineStep(cpu, 1, rom, mem);
                CPUDump(cpu, 0, rom[cpu.pc.data], mem[cpu.memory.address]);
            }
        } else if (sscanf(buffer, "rom %d %d", &address, &dsize) >= 1 || sscanf(buffer, "r %d %d", &address, &dsize) >= 1) {
            for (int a = address; a < (address + dsize); a++) {
                word_to_binary_string(rom[a], bs);
                printf("%6d: %s %6d %4X\n", a, bs, rom[a], rom[a]);
            }
        } else if (sscanf(buffer, "mem %d %d", &address, &dsize) >= 1 || sscanf(buffer, "m %d %d", &address, &dsize) >= 1) {
            for (int a = address; a < (address + dsize); a++) {
                word_to_binary_string(mem[a], bs);
                printf("%6d: %s %6d %4X\n", a, bs, (int16_t)mem[a], mem[a]);
            }
        } else if (sscanf(buffer, "memwrite %d %d", &address, &value) >= 1 || sscanf(buffer, "mw %d %d", &address, &value) >= 1) {
            mem[address] = value;
            if (getenv("S_MACHINE_BATCH") != NULL) {
                continue;
            }
            for (int a = address; a < (address + dsize); a++) {
                word_to_binary_string(mem[a], bs);
                printf("%6d: %s %6d %4X\n", a, bs, (int16_t)mem[a], mem[a]);
            }
        } else {
            printf("Unknown command: %s\n", buffer);
            continue;
        }

        command_count++;
    }

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
