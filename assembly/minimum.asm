// liddictm
// minimum.asm

// Step one is to make array of all the values

// initialize i pointer
@100	// A <- 100
D = A	// D <- A
@i		// A <- i
M = D	// i = &array

// load value 8 into array[i]
@8		// A <- 8
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 8

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 8 into array[i]
@4		// A <- 4
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 4

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 4 into array[i]
@2		// A <- 2
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 2

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 2 into array[i]
A = M	// A <- i pointer
M = 1	// M[i]

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 0 into array[i]
A = M	// A <- i pointer
M = 0	// M[i]

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load -1 into array[i]
A = M	// A <- i pointer
M = -1	// M[i]

// Step two is to find minimum value of array; store it in M[0]

// set i to start of array
@0			// A <- 100
D = A   	// D <- A
@i      	// A <- i
M = D   	// i = &array

// set min to the first value
@i
D = M
@min
M = D

@10
D = A
@placeholder
M = D

(LOOP)
@i
A = M		// A <- M[i]
D = M		// D <- M[A]
@END
D; JLT		// if (*i < 0) goto END

@i
A = M
D = M
@placeholder
D = D - A
@NEW_MIN
D; GLT		// if i - min < 0, we have a new min value

D = D - M	// D = D - M[i]
@NEW_MIN
D; JGT		// set the new minimum to M[i] if min - i < 0	

@i
M = M + 1	// i++

@i
D = A
@LOOP
D; JGE		// continue loop if i > -1

(NEW_MIN)
@i
D = M		// D <- M[i]
@min
M = D		// M[min] <- D
@i
M = M + 1
@LOOP
0; JMP		// continue loop

(END)
@min
D = M		// D <- M[min]
@0
M = D		// M[0] <- D
