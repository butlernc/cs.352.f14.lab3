// liddictm
// sorting.asm


// Step one is to make array of all the values
// initialize i pointer
@0			// A <- 100
D = A   	// D <- A
@i      	// A <- i
M = D   	// i = &array

// load 5 into array[0]
@5      	// A <- 5
D = A   	// D <- A
@i      	// A <- i
A = M   	// A <- M[i]
M = D   	// M[i] <- D (and D = 5)

// increment pointer
@i    	    // A <- i
M = M + 1 	// M[i]++

// load 2 into array[i]
@2     	 	// A <- 2
D = A   	// D <- A
@i      	// A <- i
A = M   	// A <- M[i]
M = D   	// M[i] <- D (and D = 2)

// increment pointer
@i         	// A <- i
M = M + 1  	// M[i]++

// load 4 into array[i]
@4      	// A <- 4
D = A   	// D <- A
@i      	// A <- i
A = M   	// A <- M[i]
M = D   	// M[i] <- D (and D = 4)

// increment pointer
@i       	// A <- i
M = M + 1  	// M[i]++

// load -1 into array[i] as delimiter
A = M   	// A <- i pointer
M = -1   	// M[i]

// Step two is to sort the array; store it in M[0]

// set i to start of array
@0			// A <- 100
D = A   	// D <- A
@i      	// A <- i
M = D   	// i = &array

// set sort to the first value
@i
D = M
@sort
M = D

@10
D = A
@placeholder
M = D

(LOOP)
@i
A = M		// A <- M[i]
D = M		// D <- M[A]
@END
D; JLT		// if i >= number of items, exit the loop

@i
A = M
D = M
@placeholder
D = D - A
@NEW_SORT
D; GLT		// if i == 0, move on to the next element

D = D - M	// D = D - M[i]
@NEW_SORT
D; JGT		// set the new value i - 1 with i

@i
M = M + 1	// i++

@i
D = A
@LOOP
D; JGE		// if i - 1 <= i, they are sorted so move to next element 

(NEW_SORT)
@i
D = M		// D <- M[i]
@sort
M = D		// M[sort] <- D
@i
M = M + 1
@LOOP
0; JMP		// continue loop

(END)
@sort
D = M		// D <- M[sort]
@0
M = D		// M[0] <- D