// liddictm
// arithmetic.asm

@6
D = A
@1
M = D //assign M[1] = 6
@8
D = A
@2
M = D //assign M[2] = 8
@12
D = A
@3
M = D //assign M[3] = 12
@16
D = A
@4
M = D //assign M[4] = 16


@1        // A <- 1
D = M     // D <-M[1]
@2        // A <- 2
D = D + M // D <- D + M[2]
@0        // A <- 0
M = D     // M[0] <- D
@3        // A <- 3
D = M     // D <- M[3]
@4        // A <- 4
D = D + M // D <- D + M[4]
@0        // A <- 0
M = M - D // M[0] <- M[0] - D
