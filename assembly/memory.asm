// liddictm
// memory.asm

// initialize i pointer
@100	// A <- 100
D = A	// D <- A
@i		// A <- i
M = D	// i = &array

// load value 8 into array[i]
@8		// A <- 8
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 8

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 8 into array[i]
@4		// A <- 4
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 4

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 4 into array[i]
@2		// A <- 2
D = A	// D <- A
@i		// A <- i
A = M	// A <- M[i]
M = D	// M[i] <- D
		// D = 2

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 2 into array[i]
A = M	// A <- i pointer
M = 1	// M[i]

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load 0 into array[i]
A = M	// A <- i pointer
M = 0	// M[i]

// increment pointer
@i			// A <- i
M = M + 1	// M[i]++

// load -1 into array[i]
A = M	// A <- i pointer
M = -1	// M[i]
