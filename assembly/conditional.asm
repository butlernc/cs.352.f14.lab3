// liddictm
// conditional.asm

@0
D = A
@1
M = D //assign M[1] = 0
@1
D = A
@2
M = D //assign M[2] = 1

@1		// A <- 1
D = M	// D <- M[1]
@WI		// A <- WI
D;JEQ	// if D == 0 JMP
@2		// A <- 2
D = M 	// D = M[2]
@MN		// A <- MN	
D;JEQ	// if D == 0 JMP
@3		// A <- 3
D = A	// D <- 3
@0		// A <- 0
M = D	// M[0] <- D
@END	// A <- END
0;JMP	// JMP to END	
(WI)	
@2		// A <- 2
D = M	// D <- M[2]
@CA		// A <- CA
D;JEQ	// if D == 0 JMP 
@0		// A <- 0
M = 1	// M[0] = 1
@END	// A <- END
0;JMP	// JMP to END
(MN)	
@2		// A <-2
D = A	// D <- M[2]
@0		// A <- 0
M = D	// M[0] <- 0
@END	// A <- END
0;JMP	// JMP to END
(CA)	
@0		// A <- 0
M = 0	// M[0] <- 0
@END	// A <- END
0;JMP	// JMP to END
(END)
@END	// A <- END
0;JMP	// JMP to END
